
# Theo project

Theo (Tool for HEterogeneous Optimization) is a tool that cooperates with agora to enlarge or retrigger the design exploration phase. It integrates with mARGOt and agora seamlessly since it exploits the same communication medium (MQTT).

## Installation

* It requires Python3.8;
* Create the virtual environment with "virtualenv ./env"
* Load the environment with "source env/bin/activate"
* Install the requirements with "pip3 install -r requirements.txt"

## Execution

* Load the environment with "source env/bin/activate"
* Launch Theo with "python3 main.py"

## Configuration

* The address of the MQTT broker is at the bottom of the main.py file;
* It is possible to modify the behaviour of Theo by providing configuration parameters directly in the mARGOt configuration file. Theo will search for parameters among the clustering parameters in the "agora" part. In particular, it will consider only parameters starting with "theo_".
* Parameters supported up to now:
  * "theo_trigger_method": specifies the type of condition to be checked. Supported values are "feature_distance", which will consider the distance between features, or "observation_distance", which will measure the delta between the observation and the model (default: "feature_distance").
  * "theo_trigger_delta": specifies the maximum distance that won't trigger the condition. Must be an integer (default: 10).
  * "theo_trigger_delta_type": specifies the type of the delta to be considered in "observation_distance" mode. Accepted values are "percent", which will consider the delta as a percentage, or "absolute", which will consider it as a pure differential (default: "percent").
  * "theo_trigger_obs_count": specifies the number of observations to be considered when checking the "observation_distance" condition. Must be an integer (default: 1).
  * "theo_<feature_name>": specifies a factor that will be multiplied to the delta along that features. It's useful to weight the distance on cerain features or remove themo from the computation. Must be a float (default: 1.0).
  * "theo_hw_features": specifies the set of input features to use for happiness calculations. The names of the input features must be specified in a single string, separated by '$' symbols.
  * "theo_hw_id": specifies the name of the VM hosting the application, useful for happiness logging into file.

### To be checked

* Time to knowledge;
* Multi application interactions;

### Acknowledgment

This work has been supported by European Commission under the following grants:

* EVEREST No. 957269 (dEsign enVironmEnt foR Extreme-Scale big data analyTics on heterogeneous platforms)
