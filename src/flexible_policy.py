from tinydb import TinyDB, where
import sys

def apply_policy(incoming, composite, basic, app_id):
    db = TinyDB("db/theo.json")
    entry = db.get(where("app_id") == app_id)
    policy_name = entry["policy"]
    p_module, p_func = policy_name.rsplit('.', 1)
    if p_module not in sys.modules:
        __import__(p_module)
    policy_function = getattr(sys.modules[p_module], p_func)
    return policy_function(incoming, composite, basic, app_id)
