import json
from tinydb import TinyDB, where
import sys

def welcome_message_handler(client, userdata, msg):
    exchanged = json.loads(msg.payload.decode("utf-8"))
    app_id = msg.topic.split("/")[1]
    name = app_id.split("^")[0]
    version = app_id.split("^")[1]
    block = app_id.split("^")[2]

    if "theo" in version:
        return      # Avoid circular references

    db = TinyDB("db/welcome.json")
    entries = db.search((where("name") == name) & (where("version") == version))
    theo_db = TinyDB("db/theo.json")
    if len(entries) > 0:
        print("Welcome msg of known app")
        db.update(exchanged, (where("name") == name) & (where("version") == version))
        theo_entry = theo_db.get(where("app_id") == app_id)
        theo_entry["sent_know"] = False
        theo_db.update(theo_entry, (where("app_id") == app_id))
    else:
        #first time, add to theo_db and to welcome
        print("Welcome message of new app")
        entry = dict()
        entry["app_id"] = app_id
        entry["theo_version"] = 0
        entry["exploring"] = False
        entry["sent_know"] = False
        theo_db.insert(entry)
        db.insert(exchanged)
    extract_flexible_conf(app_id, exchanged)

def extract_flexible_conf(app_id, exchanged):
    db = TinyDB("db/configs.json")
    conf_container = exchanged["blocks"][0]["agora"]["clustering_parameters"]
    entry = dict()
    entry["app_id"] = app_id
    triggers = dict()
    for parameter in conf_container: #Array of configurations
        for elem in parameter: # Each configuration is a dictionary with 1 pair
            if "theo" in elem:
                new_key = elem.split("theo_")[1]
                if new_key.startswith("trigger"):
                    parts = new_key.split("_", 2) # Parts will have 3 elements: "trigger", number, field
                    if parts[1] not in triggers:
                        triggers[parts[1]] = dict()
                    triggers[parts[1]][parts[2]] = parameter[elem]
                else:
                    entry[new_key] = parameter[elem]
    
    #Check trigger correctness, compact them and then put them in a list
    list_triggers = [(number, triggers[number]) for number in triggers]
    number_list = [(int(number), val) for (number,val) in list_triggers if number.isdigit()]
    number_list.sort()
    result_list = []
    for item in number_list:
        if ("type" not in item[1]) or ("policy" not in item[1]):
            continue
        
        #check trigger existence
        trigger_name = item[1]["type"]
        policy_name = item[1]["policy"]
        if '.' in trigger_name:
            module, func = trigger_name.rsplit('.', 1)
            if module not in sys.modules:
                __import__(module)
            trigger_function = getattr(sys.modules[module], func, None)
            if trigger_function == None:
                print("Trigger function " + item[0] + " unsupported. Name: " + trigger_name)
                continue
        else:
            print("Trigger " + item[0] + " type must contain a valid path with a module. Value provided: " + trigger_name)
            continue

        if '.' in policy_name:
            p_module, p_func = policy_name.rsplit('.', 1)
            if p_module not in sys.modules:
                __import__(p_module)
            policy_function = getattr(sys.modules[p_module], p_func, None)
            if policy_function == None:
                print("Policy function " + item[0] + " unsupported. Name: " + policy_name)
                continue
        else:
            print("Policy " + item[0] + " type must contain a valid path with a module. Value provided: " + policy_name)
            continue

        #If still there, then entry ok
        result_list.append(item[1])

    entry["triggers"] = result_list
    old_entries = db.search(where("app_id") == app_id)
    if len(old_entries) == 0:
        db.insert(entry)
    else:
        db.update(entry, where("app_id") == app_id)
    