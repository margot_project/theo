from src.flexible_triggers import check_trigger
from tinydb import TinyDB, where
import json

def observation_message_handler(client, userdata, msg):
    if "@" in msg.payload.decode("utf-8"):
        exchanged = json.loads(msg.payload.decode("utf-8").split("@")[2])
        with_char = True
    else:
        exchanged = json.loads(msg.payload.decode("utf-8"))
        with_char = False
    app_id = msg.topic.split("/")[1]
    t_id = msg.topic.split("/")[3]
    name = app_id.split("^")[0]
    version = app_id.split("^")[1]
    block = app_id.split("^")[2]
    #if "theo" in version:
    #    return              #Avoid circular references

    print("Found obs")
    theo_db = TinyDB("db/theo.json")
    entry = theo_db.get(where("app_id") == app_id)
    if not entry["exploring"]:
        #check trigger on standard knowledge
        know_db = TinyDB("db/know/" + app_id + ".json")
        basic_knowledge = know_db.get(doc_id = 2)
        if not check_trigger(exchanged, app_id, basic_knowledge):
            #check trigger on composed knowledge
            print("Trigger failed on basic know")
            composed_knowledge = know_db.get(doc_id = 1)
            if not check_trigger(exchanged, app_id, composed_knowledge):
                #need for retraining
                print("Trigger failed on composite know")
                welcome_db = TinyDB("db/welcome.json")
                welcome_msg = welcome_db.get((where("name") == name) & (where("version") == version))
                #welcome_msg["version"] += "theo" + str(entry["theo_version"])
                new_app_id = name + "^" + welcome_msg["version"] + "^" + block
                client.publish("margot/" + new_app_id + "/clear/" + t_id, "clear plz")
                client.publish("margot/" + new_app_id + "/welcome/" + t_id, json.dumps(welcome_msg))
                entry["exploring"] = True
                theo_db.update(entry, where("app_id") == app_id)
            else:
                if not entry["sent_know"]:
                    print("Forwarding composite know")
                    #send composed_knowledge
                    #Maybe wait first
                    client.publish("margot/" + app_id + "/" + t_id + "/prediction", json.dumps(composed_knowledge))
                    entry["sent_know"] = True
                    theo_db.update(entry, where("app_id") == app_id)
                else:
                    print("Composite know already sent")
                    pass
        else:
            print("Basic know ok")
            #do nothing
            pass
        suggest_happiness(exchanged, client, app_id, t_id)
    #else:
        #forward to agora
        #agora gets them by default
        #print("Forwarding observation to agora")
        #new_version = version + "theo" + str(entry["theo_version"])
        #new_app_id = name + "^" + new_version + "^" + block
        #if with_char:
        #    message = msg.payload.decode("utf-8").split("@")[0] + "@" + msg.payload.decode("utf-8").split("@")[1] + "@" + json.dumps(exchanged)
        #else:
        #    message = json.dumps(exchanged)
        #client.publish("margot/" + new_app_id + "/observation/" + t_id, message)
    
def suggest_happiness(observation, client, app_id, t_id):
    block = app_id.split("^")[2]
    config_db = TinyDB("db/configs.json")
    config = config_db.get(where("app_id") == app_id)
    if "hw_features" not in config or config["hw_features"] == "":
        return
    hw_list = config["hw_features"].split("$",-1)
    hw_list.sort()

    obs_compact = dict()
    for op in observation[block]:
        for kind in ["features", "knobs", "metrics"]:
            for key in op[kind]:
                if type(op[kind][key]) is list:
                    obs_compact[key] = op[kind][key][0]
                else:
                    obs_compact[key] = op[kind][key]

    hap_db = TinyDB("db/know/"+ app_id + "_hap.json")
    hap_list = hap_db.get(doc_id=1)
    cur_hap = None
    for i in hap_list["data"]:
        if cur_hap:
            break
        cur_hap = i
        for hw in hw_list:
            if obs_compact[hw] != cur_hap[hw]:
                cur_hap = None
                break
    
    result = []
    for i in hap_list["data"]:
        if i == cur_hap:
            continue
        temp = i.copy()
        temp["happiness"] = temp["normalised_happiness"] - cur_hap["normalised_happiness"]
        del temp["normalised_happiness"]
        result.append(temp)
    
    if "hw_id" in config and not config["hw_id"] == "":
        with open("/tmp/vm_happiness.json", "w+") as f:
            try:
                current_dict = json.load(f)
            except:
                current_dict = {}
            current_dict[config["hw_id"]] = result
            json.dump(current_dict,f)
    client.publish("margot/" + app_id + "/" + t_id + "/happiness", json.dumps(result))