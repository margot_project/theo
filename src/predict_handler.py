import json
from tinydb import TinyDB, where
from tinydb.table import Document
from src.flexible_policy import apply_policy


def prediction_message_handler(client, userdata, msg):
    print("Predict here")
    topic_path = msg.topic.split("/")
    if "prediction" in topic_path[2]:
        global_predict_handler(client, userdata, msg)
    else:
        single_predict_handler(client, userdata, msg)

def global_predict_handler(client, userdata, msg):
    exchanged = json.loads(msg.payload.decode("utf-8"))
    app_id = msg.topic.split("/")[1]
    name = app_id.split("^")[0]
    original_version = app_id.split("^")[1]
    block = app_id.split("^")[2]
    #if "theo" in version:
    #    print("Strange...")
    #    original_version = version.split("theo")[0]
    #else:
    #    original_version = version
    original_app_id = name + "^" + original_version + "^" + block
    db = TinyDB("db/know/" + original_app_id + ".json")
    if len(db.all()) == 0:
        db.insert(exchanged)
        db.insert(Document(exchanged, doc_id = 2))
        produce_happiness(exchanged, original_app_id)
    else:
        composite_knowledge = db.get(doc_id = 1)
        basic_knowledge = db.get(doc_id = 2)
        composite_knowledge[block], basic_knowledge[block] = apply_policy(exchanged[block], composite_knowledge[block], basic_knowledge[block], original_app_id)
        db.update(composite_knowledge, doc_ids = [1])
        db.update(basic_knowledge, doc_ids = [2])
        #stored_knowledge[block].extend(exchanged[block])
        #db.update(stored_knowledge, doc_ids = [1])

        # Remove observations since they refer to a different knowledge
        delta_db = TinyDB("db/delta/" + original_app_id + ".json")
        delta_db.truncate()

        #forward to application
        #client.publish("margot/" + original_app_id + "/prediction", json.dumps(composite_knowledge))

        #remove exploration flag from theo db
        theo_db = TinyDB("db/theo.json")
        entry = theo_db.get(where("app_id") == original_app_id)
        #entry["theo_version"] += 1
        entry["exploring"] = False
        theo_db.update(entry, where("app_id") == original_app_id)
        produce_happiness(composite_knowledge, original_app_id)
        

def single_predict_handler(client, userdata, msg):
    exchanged = json.loads(msg.payload.decode("utf-8"))
    app_id = msg.topic.split("/")[1]
    t_id = msg.topic.split("/")[2]
    name = app_id.split("^")[0]
    original_version = app_id.split("^")[1]
    block = app_id.split("^")[2]
    #if "theo" in version:
    #    print("Strange...")
    #    original_version = version.split("theo")[0]
    #else:
    #    original_version = version
    original_app_id = name + "^" + original_version + "^" + block
    db = TinyDB("db/know/" + original_app_id + ".json")
    if len(db.all()) == 0:
        db.insert(exchanged)
        db.insert(Document(exchanged, doc_id = 2))
        produce_happiness(exchanged, original_app_id)
    else:
        theo_db = TinyDB("db/theo.json")
        entry = theo_db.get(where("app_id") == original_app_id)
        #entry["theo_version"] += 1
        if entry["exploring"]:
            entry["exploring"] = False
            theo_db.update(entry, where("app_id") == original_app_id)
            #add to db
            composite_knowledge = db.get(doc_id = 1)
            basic_knowledge = db.get(doc_id = 2)
            composite_knowledge[block], basic_knowledge[block] = apply_policy(exchanged[block], composite_knowledge[block], basic_knowledge[block], original_app_id)
            db.update(composite_knowledge, doc_ids = [1])
            db.update(basic_knowledge, doc_ids = [2])
            #stored_knowledge[block].extend(exchanged[block])
            #db.update(stored_knowledge, doc_ids = [1])

            # Remove observations since they refer to a different knowledge
            delta_db = TinyDB("db/delta/" + original_app_id + ".json")
            delta_db.truncate()
            produce_happiness(composite_knowledge, original_app_id)

            #forward to application
            #client.publish("margot/" + original_app_id + "/" + t_id + "/prediction", json.dumps(composite_knowledge))


def geo_mean(structure, config):
    total = 1
    for elem in structure:
        total *= config[elem]**structure[elem]
    return total

def lin_mean(structure, config):
    total = 0
    for elem in structure:
        total += config[elem]*structure[elem]
    return total

def produce_happiness(knowledge, app_id):
    # Getting list of features to vary in our analysis
    config_db = TinyDB("db/configs.json")
    config = config_db.get(where("app_id") == app_id)
    if "hw_features" not in config or config["hw_features"] == "":
        return
    hw_list = config["hw_features"].split("$",-1)
    hw_list.sort()

    # Getting optimisation problem and constraints
    welcome_db = TinyDB("db/welcome.json")
    name = app_id.split("^")[0]
    version = app_id.split("^")[1]
    block = app_id.split("^")[2]
    entry = welcome_db.get((where("name") == name) & (where("version") == version))
    opt_problem = entry["blocks"][0]["extra-functional_requirements"][0]
    if "minimize" in opt_problem:
        text_direction = "minimize"
        direction = lambda x : 1/x
    elif "maximize" in opt_problem:
        text_direction = "maximize"
        direction = lambda x : x
    else:
        return
    obj_function = opt_problem[text_direction]
    if "geometric_mean" in obj_function:
        text_combination = "geometric_mean"
        combination = geo_mean
    elif "linear_mean" in obj_function:
        text_combination = "linear_mean"
        combination = lin_mean
    else:
        return
    obj_function_structure = dict()
    for pair in obj_function[text_combination]:
        for key in pair:
            obj_function_structure[key] = int(pair[key])

    #TODO handle constraints

    #Format knowledge & compute happiness
    all_entries = []
    for op in knowledge[block]:
        compact_entry = dict()
        for kind in ["features", "knobs", "metrics"]:
            for key in op[kind]:
                if type(op[kind][key]) is list:
                    compact_entry[key] = op[kind][key][0]
                else:
                    compact_entry[key] = op[kind][key]
        compact_entry["theo_happiness"] = combination(obj_function_structure, compact_entry)
        all_entries.append(compact_entry)

    #Get possible values of hw_features
    hw_feature_values = dict()
    for feature in hw_list:
        seen_values = []
        for op in all_entries:
            if op[feature] not in seen_values:
                seen_values.append(op[feature])
        hw_feature_values[feature] = seen_values

    def combo_gen(hw_list_p, hw_feature_values_p):
        if hw_list_p:
            for item in hw_feature_values_p[hw_list_p[0]]:
                dict_temp = dict()
                dict_temp[hw_list_p[0]] = item
                if len(hw_list_p) == 1:
                    yield dict_temp
                else:
                    for combo in combo_gen(hw_list_p[1:], hw_feature_values_p):
                        temp2 = dict_temp.copy()
                        temp2.update(combo)
                        yield temp2

    # Best configurations returns the knowledge condensed to the hw_features, considering only best configurations
    best_configurations = []
    for point in combo_gen(hw_list, hw_feature_values):
        compatible = [op for op in all_entries for key in point if op[key] == point[key]]
        optimal = [op for op in compatible if direction(op["theo_happiness"]) == max([direction(op1["theo_happiness"]) for op1 in compatible])]
        best_configurations.append(optimal[0])
    
    max_happiness = max([direction(op["theo_happiness"]) for op in best_configurations])
    for point in best_configurations:
        point["normalised_happiness"] = round(direction(point["theo_happiness"]) / max_happiness,6)

    condensed = []
    for point in best_configurations:
        temp = dict()
        for item in hw_list:
            temp[item] = point[item]
        temp["normalised_happiness"] = point["normalised_happiness"]
        condensed.append(temp)

    entry = {"data":condensed}
    hap_db = TinyDB("db/know/"+ app_id + "_hap.json")
    if len(hap_db.all()) == 0:
        hap_db.insert(entry)
    else:
        hap_db.update(entry, doc_ids=[1])
    print("Produced happiness")