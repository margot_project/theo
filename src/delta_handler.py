import json
from tinydb import TinyDB


def delta_message_handler(client, userdata, msg):
    exchanged = json.loads(msg.payload.decode("utf-8"))
    app_id = msg.topic.split("/")[1]
    version = app_id.split("^")[1]

    if "theo" in version:
        #It should not happen, just to be sure
        return

    delta_db = TinyDB("db/delta/" + app_id + ".json")
    if "delta" not in exchanged:
        #Bad message, ignoring
        return

    cleaned_delta = []
    for entry in exchanged["delta"]: #Array of dicts, entry is just a dict with a single element
        for key in entry: #key will hold the name of the metric
            if entry[key] != "N/A":
                cleaned_delta.append(entry)
    
    if cleaned_delta != []:
        delta_db.insert({"delta":cleaned_delta})
            
    