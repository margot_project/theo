# The trigger activates when the observed input feature get too far from the known values
# It follows Manhattan distance
# Parameters:
#   "border": maximum distance without triggering (default: 10)
#   <input_feature_name>: factor that multiplies the distance on the given dimension (default: 1)

def distance(observation, app_id, knowledge, configs):
    if "border" not in configs:
        border = 10
    else:
        border = configs["border"]
    block = app_id.split("^")[2]
    for event in observation[block]:
        if "features" in event:
            feature_list = event["features"]
        else:
            #no features, distance always ok
            return True
        for op in knowledge[block]:
            op_feature_list = op["features"]
            distance = 0
            for i in feature_list:
                difference = abs(feature_list[i] - op_feature_list[i])
                try:
                    factor = float(configs[i])
                except:
                    factor = 1
                distance += difference * factor
            if distance <= border:
                print("OK")
                return True
    print("KO")
    return False