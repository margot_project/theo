from tinydb import TinyDB, where, Query
import functools
from scipy import stats


# The trigger activates when the observed values get too far from the expected ones.
# It considers per-metric distance.
# If the average distance over the observation window is bigger than the threshold, then the trigger is activated.
# Parameters:
#   "obs_count": dimension of the observation_window (default: 1)
#   "delta_type": how to measure the distance between expected and actual (either "percent" or "absolute") (default: "percent")
#   <metric_name>: maximum distance without activating the trigger

def distance(observation, app_id, knowledge, configs):
    if "obs_count" not in configs:
        obs_count = 1
    else:
        obs_count = configs["obs_count"]

    if "delta_type" not in configs:
        delta_type = "percent"
    else:
        delta_type = configs["delta_type"]

    delta_db = TinyDB("db/delta/" + app_id + ".json")
    obs_number = delta_db.count(Query().noop())
    if obs_number < obs_count:
        print("Not enough deltas")
        return True
    distances = dict()
    for i in range(obs_number + 1 - obs_count, obs_number + 1):
        delta = delta_db.get(doc_id = i)["delta"]
        for metric in delta:
            if delta[metric] == "N/A":
                continue
            if metric not in distances:
                distances[metric] = []
            if delta_type == "absolute":
                distances[metric].append(abs(delta[metric][0] - delta[metric][1]))
            elif delta_type == "percent":
                if delta[metric][0] != 0:
                    distances[metric].append(abs(delta[metric][0] - delta[metric][1])/delta[metric][0])
                else:
                    distances[metric].append(abs(delta[metric][0] - delta[metric][1]))
    
    problematic_list = []
    for metric in distances:
        sum = functools.reduce(lambda a,b : a+b, distances[metric])
        average = sum / len(distances[metric])
        try:
            border = float(configs[metric])
            if border < average:
                problematic_list.append(metric)
        except:
            pass
        
    if len(problematic_list) == 0:
        print ("No problems")
        return True
    else:
        print ("Some metrics are too distant:" + problematic_list)
        return False


# The trigger activates when the observed values get too far from the expected ones.
# It considers per-metric distance.
# If the distances don't pass the t_test over the last observations are bigger than the threshold, then the trigger is activated.
# Parameters:
#   "obs_count_max": maximum amount of observations to consider. If 0 it will consider all observations (default: 0)
#   <metric_name>: maximum distance without activating the trigger

def ttest(observation, app_id, knowledge, configs):
    if "obs_count_max" not in configs:
        obs_count_max = 0
    else:
        obs_count_max = configs["obs_count_max"]

    delta_db = TinyDB("db/delta/" + app_id + ".json")

    obs_number = delta_db.count(Query().noop())
    if obs_number > obs_count_max and obs_count_max != 0:
        required = range(obs_number - obs_count_max + 1, obs_number + 1)
        #Take last obs_count_max
        pass
    else:
        #Take all
        required = range(1, obs_number + 1)
        pass

    distances = dict()
    for i in required:
        delta = delta_db.get(doc_id = i)["delta"]
        for metric in delta:
            if delta[metric] == "N/A":
                continue
            if metric not in distances:
                distances[metric] = []
            distances[metric].append(delta[metric][0] - delta[metric][1])

    problematic_list = []
    for metric in distances:
        try:
            result = stats.ttest_1samp(distances[metric], 0.0)
            threshold = configs[metric]
            if result < float(threshold):
                problematic_list.append(metric)
        except:
            pass

    if len(problematic_list) == 0:
        print ("No problems")
        return True
    else:
        print ("Some metrics fail the t test:" + problematic_list)
        return False

    