from tinydb import TinyDB, where
import sys

def check_trigger(observation, app_id, knowledge):
    if knowledge == None:
        return True
    elif knowledge == []:
        return False
    db = TinyDB("db/configs.json")
    entry = db.get(where("app_id") == app_id)
    for trigger in entry["triggers"]:
        trigger_name = trigger["type"]
        module, func = trigger_name.rsplit('.', 1)
        if module not in sys.modules:
            __import__(module)
        trigger_function = getattr(sys.modules[module], func)
        result = trigger_function(observation, app_id, knowledge, trigger)
        if result:
            continue
        else:
            theo_db = TinyDB("db/theo.json")
            theo_entry = theo_db.get(where("app_id") == app_id)
            theo_entry["policy"] = trigger["policy"]
            theo_db.update(theo_entry, where("app_id") == app_id)
            return False
    return True