import paho.mqtt.client as mqtt
import json
import time
import argparse
from tinydb import TinyDB, where
from src.welcome_handler import welcome_message_handler
from src.info_handler import info_message_handler
from src.obs_handler import observation_message_handler
from src.predict_handler import prediction_message_handler
from src.explore_handler import explore_message_handler
from src.delta_handler import delta_message_handler


def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    client.subscribe("margot/+/welcome/#")
    client.subscribe("margot/+/info/#")
    client.subscribe("margot/+/observation/#")

    client.subscribe("margot/+/prediction")
    client.subscribe("margot/+/+/explore")
    client.subscribe("margot/+/+/prediction")
    client.subscribe("margot/+/delta/#")


def message_dispatcher(client, userdata, msg):
    topic_path = msg.topic.split("/")
    if "welcome" in topic_path[2]:
        welcome_message_handler(client, userdata, msg)
    elif "info" in topic_path[2]:
        info_message_handler(client, userdata, msg)
    elif "observation" in topic_path[2]:
        observation_message_handler(client, userdata, msg)
    elif "prediction" in topic_path[2] or "prediction" in topic_path[3]:
        prediction_message_handler(client, userdata, msg)
    elif "explore" in topic_path[3]:
        explore_message_handler(client, userdata, msg)
    elif "delta" in topic_path[2]:
        delta_message_handler(client, userdata, msg)

"""
def on_message(client, userdata, msg):
    print(msg.topic+" "+msg.payload.decode("utf-8"))
    if "INJECTED" not in msg.payload.decode("utf-8"):
        client.publish(msg.topic, '{"message": "HEY I AM INJECTED!!! ^.^"}'.encode("utf-8"))
"""

def on_message(client, userdata, msg):
    if "welcome" in msg.topic:
        exchanged = json.loads(msg.payload.decode("utf-8"))
        app_id = msg.topic.split("/")[1]
        name = app_id.split("^")[0]
        version = app_id.split("^")[1]
        block = app_id.split("^")[2]

        if "theo" in version:
            return      # Avoid circular references

        db = TinyDB("db/apps.json")
        db.insert(exchanged)

        new_app_id = name + "^" + version + "theo^" + block
        t_id = msg.topic.split("/")[3]
        exchanged["version"] += "theo"
        time.sleep(20)
        client.publish("margot/" + new_app_id + "/welcome/" + t_id, json.dumps(exchanged))
        
    elif "explore" in msg.topic:
        print (msg.topic + "          " + msg.payload.decode("utf-8"))


parser = argparse.ArgumentParser(
    description="Execute the Theo component, responsible for the observation of the communication between AGORA and mARGOt."
)
parser.add_argument("mqtt_broker_address", help="Address of the MQTT brokerpy", default="127.0.0.1")

args = parser.parse_args()

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = message_dispatcher
client.connect(args.mqtt_broker_address, 1883, 60)

client.loop_forever()

